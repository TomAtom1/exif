<?php

namespace Exif;

class Exif {

  protected $exifData = [];
  
  public function __constructor(string $fileName = null) {
    if ($fileName) {
      $this->loadFile();
    }
  }

  public function loadFile(string $fileName) {
    $this->exifData = [];
    if (file_exists($fileName)) {
      $fileData = exif_read_data($fileName);
      if (\is_array($fileData)) {
        $this->exifData = $fileData;
      }
    } else {
      throw new \InvalidArgumentException('Soubor ' . $fileName . ' neexistuje.');
    }
  }

  public function haveAnyData(): bool {
    if ($this->getExposureTime() !== null ||
            $this->getFNumber() !== null ||
            $this->getIso() !== null ||
            $this->getFocalLength() !== null ||
            $this->getFocal35() !== null ||
            $this->getExposureBiasValue() !== null ||
            $this->getDateTime() !== null ||
            $this->getMake() !== null ||
            $this->getModel() !== null) {
      return true;
    } else {
      return false;
    }
  }

  public function getAllExifData(): array {
    return $this->exifData;
  }

  private function stringToFloat(string $val): ?float {
    $tmp = explode('/', $val);
    if (count($tmp) == 2 && is_numeric($tmp[0]) && is_numeric($tmp[1])) {
      return (float) $tmp[0] / $tmp[1];
    }
    return null;
  }

  public function getFocalLength(): ?float {
    if (isset($this->exifData['FocalLength']) && $this->exifData['FocalLength'] !== '') {
      return round($this->stringToFloat($this->exifData['FocalLength']), 0);
    }
    return null;
  }

  public function getFNumber(): ?float {
    if (isset($this->exifData['FNumber'])) {
      return $this->stringToFloat($this->exifData['FNumber']);
    }
    return null;
  }

  public function getExposureBiasValue(): ?float {
    if (isset($this->exifData['ExposureBiasValue']) && $this->exifData['ExposureBiasValue'] !== '') {
      return number_format($this->stringToFloat($this->exifData['ExposureBiasValue']), 2, '.', '');
    }
    return null;
  }

  public function getExposureTime(): ?string {
    if (isset($this->exifData['ExposureTime']) && $this->exifData['ExposureTime'] !== '') {
      $time = $this->stringToFloat($this->exifData['ExposureTime']);
      if ($time < 1) {
        return $time = '1/' . (1 / $time);
      }
      return $time;
    }
    return null;
  }

  public function getIso(): ?int {
    if (isset($this->exifData['ISOSpeedRatings']) && $this->exifData['ISOSpeedRatings'] !== '') {
      return (int) $this->exifData['ISOSpeedRatings'];
    }
    return null;
  }

  public function getMake(): ?string {
    if (isset($this->exifData['Make']) && $this->exifData['Make'] !== '') {
      return $this->exifData['Make'];
    }
    return null;
  }

  public function getModel(): ?string {
    if (isset($this->exifData['Model']) && $this->exifData['Model'] !== '') {
      return $this->exifData['Model'];
    }
    return null;
  }

  public function getFocal35(): ?int {
    if (isset($this->exifData['FocalLengthIn35mmFilm']) && $this->exifData['FocalLengthIn35mmFilm'] !== '') {
      return (int) $this->exifData['FocalLengthIn35mmFilm'];
    }
    return null;
  }

  public function getDateTime(): ?\DateTime {
    if (isset($this->exifData['DateTimeOriginal'])) {
      $dateTime = \DateTime::createFromFormat('Y:m:d H:i:s', $this->exifData['DateTimeOriginal']);
      if (is_object($dateTime)) {
        return $dateTime;
      }
    }
    return null;
  }

}