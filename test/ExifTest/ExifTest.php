<?php

namespace ExifTest;

class ExifTest extends \PHPUnit\Framework\TestCase {
  
  private $exif = null;

    private function getExif() {
      if ($this->exif === null) {
        $exif = new \Exif\Exif();
        $exif->loadFile(__DIR__ . '/photos/exif_test.jpg');
      }
      return $exif;
    }

    public function testHaveAnyData() {
        $exif = $this->getExif();
        $this->assertTrue($exif->haveAnyData());
    }
    
    public function testHaveNoneData() {
        $exif = new \Exif\Exif();
        $this->assertFalse($exif->haveAnyData());
    }

    public function testFocalLenght() {
        $exif = $this->getExif();
        $this->assertEquals(19, $exif->getFocalLength());
    }
    
    public function testFNumber() {
        $exif = $this->getExif();
        $this->assertEquals(6.3, $exif->getFNumber());
    }
    
    public function testExposureBias() {
        $exif = $this->getExif();
        $this->assertEquals(-1.00, $exif->getExposureBiasValue());
    }
    
    public function testExposureTime() {
        $exif = $this->getExif();
        $this->assertEquals('1/30', $exif->getExposureTime());
    }
    
    public function testIso() {
        $exif = $this->getExif();
        $this->assertEquals(100, $exif->getIso());
    }
    
    public function testMake() {
        $exif = $this->getExif();
        $this->assertEquals('NIKON CORPORATION', $exif->getMake());
    }
    
    public function testModel() {
        $exif = $this->getExif();
        $this->assertEquals('NIKON D60', $exif->getModel());
    }
    
    public function testFocal35() {
        $exif = $this->getExif();
        $this->assertEquals(28, $exif->getFocal35());
    }
    
    public function testDateTime() {
        $exif = $this->getExif();
        $this->assertEquals('29.12.2012 17:15:27', $exif->getDateTime()->format('j.n.Y H:i:s'));
    }

}